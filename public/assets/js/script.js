console.log("Start of the Script :)")


/* SETTING UP CANVAS */

const canvas = document.getElementById("the-canvas");
const cxt = canvas.getContext("2d");

/* VARIABLES */

//Player Varibles
let shipXPos = 150;
let shipYPos = (canvas.height / 2); // initial x & values for the ship
let shipSpeed = 3;
let shipSize = 16;


/* GAME OBJECT CREATION*/
let playerShip = document.getElementById("test-ship"); // sprite used for testing
let bullet = document.getElementById("test-bullet"); //testing sprite for bullet


/* INPUT FUNCTIONS */

//Gamer Input, takes input & converts to string
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input


//Button Event
function shootButton() //function when A is pressed
{
    gamerInput = new GamerInput("shootButton");
    setInterval(function () {element.innerHTML += "Hello"}, 1000);
    console.log("a pressed");
}
function noMovement()
{
    gamerInput = new GamerInput("None");
    console.log("button release");
}

function input(event) 
    {
    
        if (event.type === "keydown") 
        {
            switch (event.keyCode) 
            {
                case 87: //W
                    gamerInput = new GamerInput("Up")
                    break;
                case 68: // D
                    gamerInput = new GamerInput("Right")
                    break;
                case 65: // A
                    gamerInput = new GamerInput("Left")
                    break;
                case 83: //S
                    gamerInput = new GamerInput("Down")
                    break;
                case 13: //enter
                    gamerInput = new GamerInput ("shootButton")
                    break;
                default:
                    gamerInput = new GamerInput("None"); //No Input
            }
        } 
        else 
        {
            gamerInput = new GamerInput("None");
        }

    }

//On-screen joystick using the nippleJS library
//Lib from: 

var joystick = nipplejs.create
({
    zone: document.getElementById('joystick'),
    mode: 'static',
    position: {left: '7%', bottom: '18%', },
    color: 'red',
});

//nipple.on('start move end dir plain', function (evt) {
    joystick.on('dir:up', function (evt, data) 
    {
        gamerInput = new GamerInput("Up");
    });
    joystick.on('dir:down', function (evt, data) 
    {
        gamerInput = new GamerInput("Down");
    });
    joystick.on('dir:left', function (evt, data) 
    {
        gamerInput = new GamerInput("Left");
    });
    joystick.on('dir:right', function (evt, data) 
    {
        gamerInput = new GamerInput("Right");
    });
    joystick.on('end', function (evt, data) 
    {
        gamerInput = new GamerInput("None");
    });


/* DRAWING & ANIMATION FUNCTIONS */ 

function draw()
{
    cxt.clearRect(0, 0, canvas.width, canvas.height);
    cxt.drawImage(playerShip, shipXPos, shipYPos, shipSize, shipSize);
}


/* COLLISION & BULLET FUNCTIONS */

function edgeDetection() //you think you know me BWABWABWABWBBWA
{
    if (shipXPos <=0)
    {
        noMovement();
        shipXPos += 1;
    }
    if (shipXPos > (canvas.width - shipSize))
    {
        noMovement();
        shipXPos -= 1;
    }
    if(shipYPos <= 0)
    {
        noMovement();
        shipYPos += 1
    }
    if(shipYPos >= (canvas.height - shipSize))
    {
        noMovement();
        shipYPos -= 1;
    }
}

function moveBullet()
{
    let bulletXPos = shipXPos + (shipSize / 2);
    let bulletYPos = shipYPos + (shipSize / 2);
    
    while(bulletXPos < canvas.width)
    {   
        cxt.clearRect(bulletXPos, bulletYPos, 4, 4)
        cxt.drawImage(bullet, bulletXPos, bulletYPos, 4, 4)
        bulletXPos += 3;
    }

    
}


/* HEALTH, SCORE, UI */


/* UPDATE FUNCTION */

function update()
{
    if (gamerInput.action === "shootButton") //A Button
    {
        // a button
        moveBullet();
        console.log("A Pressed")
    }


        if(gamerInput.action === "Left")
        {
            shipXPos -= shipSpeed;
        }
        if(gamerInput.action === "Right")
        {
            shipXPos += shipSpeed;
        }
        if(gamerInput.action === "Down")
        {
            shipYPos += shipSpeed;
        }
        if(gamerInput.action === "Up")
        {
            shipYPos -= shipSpeed;
        }
    }


/* GAME LOOP */

function gameloop()
{
    edgeDetection();
    draw();
    update();
    window.requestAnimationFrame(gameloop);
}

window.requestAnimationFrame(gameloop);
window.addEventListener('keydown',input);
window.addEventListener('keyup',input);

